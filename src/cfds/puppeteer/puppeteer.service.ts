import { Injectable } from '@nestjs/common';
const puppeteer = require('puppeteer');

@Injectable()
export class PuppeteerService {

    private oil_url: string = 'https://markets.businessinsider.com/commodities/oil-price?type=brent';

    async scrapperPrice(assetId: string): Promise<string> {
        let price: string;

        switch (assetId) {
            case "1":
                price = await this.scrap(this.oil_url);
        }

        console.log(price)

        return price;
    }

    async scrap(web_url: string): Promise<string> {
        let browser = await puppeteer.launch();
        let page = await browser.newPage();
        await page.goto(web_url, { waitUntil: 'networkidle2', timeout: 0 });
        let price = await page.evaluate(() => {
            let data = document.querySelector('span[class="push-data "]').innerHTML;
            return data;
        });

        await browser.close();

        return price;
    }
}
