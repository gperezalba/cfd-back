import { Injectable } from '@nestjs/common';
const Web3 = require('web3');
const Tx = require('ethereumjs-tx').Transaction;
const Common = require('ethereumjs-common');
const BigNumber = require('bignumber.js');

import { Cfd } from './cfd.model';
import { format } from 'path';
import { resolve } from 'dns';

const MAX_CHECKS_PER_TX = 25;

const cfdABI = [{"constant":true,"inputs":[],"name":"seller","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_new","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"initialPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"leverage","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOracle","type":"address"}],"name":"changePriceOracle","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"asset","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"withdrawl","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"sellerPosition","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"investment","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"expiration","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"killAfterClose","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"updatePrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyer","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"matched","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"matchCFD","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"paySellerInvestment","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[],"name":"isClosed","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"checkClose","outputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"bool"},{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"open","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_seller","type":"address"},{"name":"_asset","type":"uint256"},{"name":"_price","type":"uint256"},{"name":"_investment","type":"uint256"},{"name":"_leverage","type":"uint256"},{"name":"_expiration","type":"uint256"},{"name":"position","type":"bool"},{"name":"oracleAddress","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"percentage","type":"uint256"},{"indexed":false,"name":"profit","type":"bool"},{"indexed":false,"name":"close","type":"bool"}],"name":"PriceUpdate","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"winner","type":"address"},{"indexed":false,"name":"loser","type":"address"},{"indexed":false,"name":"amount","type":"uint256"},{"indexed":false,"name":"percentage","type":"uint256"},{"indexed":false,"name":"price","type":"uint256"}],"name":"Close","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"old","type":"address"},{"indexed":true,"name":"current","type":"address"}],"name":"NewOwner","type":"event"}];
const cfdFactoryABI = [{"constant":false,"inputs":[{"name":"_cfd","type":"address[]"}],"name":"killCFD","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_new","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"withdrawFunds","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOracle","type":"address"}],"name":"changePriceOracle","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getCFDs","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"initial","type":"uint256"},{"name":"last","type":"uint256"}],"name":"checkClose","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_asset","type":"uint256"},{"name":"_investment","type":"uint256"},{"name":"_leverage","type":"uint256"},{"name":"_expiration","type":"uint256"},{"name":"position","type":"bool"}],"name":"createCFD","outputs":[{"name":"","type":"address"}],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"}],"name":"changePrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_cfd","type":"address[]"}],"name":"updatePriceOracle","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"cfds","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"indexOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_price","type":"uint256"},{"name":"_priceOracle","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"cfdAddress","type":"address"},{"indexed":true,"name":"seller","type":"address"},{"indexed":true,"name":"asset","type":"uint256"},{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"investment","type":"uint256"},{"indexed":false,"name":"leverage","type":"uint256"},{"indexed":false,"name":"expiration","type":"uint256"},{"indexed":true,"name":"position","type":"bool"}],"name":"NewCFD","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"old","type":"address"},{"indexed":true,"name":"current","type":"address"}],"name":"NewOwner","type":"event"}];
const cfdPriceOracleABI = [{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"priceByAsset","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_new","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newAssetId","type":"uint256"}],"name":"pushNewAsset","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"assetsIdArray","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_assets","type":"uint256[]"},{"name":"prices","type":"uint256[]"}],"name":"setPrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_asset","type":"uint256"}],"name":"getPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"assetsId","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"asset","type":"uint256[]"},{"indexed":false,"name":"price","type":"uint256[]"}],"name":"Price","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"old","type":"address"},{"indexed":true,"name":"current","type":"address"}],"name":"NewOwner","type":"event"}];
const cfdFactoryADDRESS = "0x470046f3866d5f1d66f06b26d4d8035a9aa0d62c";
const cfdPriceOracleADDRESS = "0x5a6b52ec980523f056d320f54705118d65512cc6";

@Injectable()
export class CfdsService {
    private web3: any = new Web3(new Web3.providers.HttpProvider("http://81.46.240.151:8545"));
    private customCommon: any = Common.default.forCustomChain('mainnet', { name: 'Pi-Chain', networkId: 8995, chainId: 8995 }, 'petersburg');
    private cfdContract: any;
    private cfdPriceOracle: any = new this.web3.eth.Contract(cfdPriceOracleABI, cfdPriceOracleADDRESS);
    private cfdFactory: any = new this.web3.eth.Contract(cfdFactoryABI, cfdFactoryADDRESS);
    private cfds: Cfd[] = [];

    async getPrice(asset: string): Promise<string> {
        return await this.cfdPriceOracle.methods.getPrice(asset).call();
    }

    async getAssets(): Promise<string []> {
        return await this.cfdPriceOracle.methods.assetsId().call();
    }

    async setPrice(assets: string[], prices: string[]): Promise<any> {
        let pricesBN: string[] = [];

        for (let i: number = 0; i < prices.length; i++) {
            let numberBN = new BigNumber(String(parseFloat(prices[i]) * 1e18));
            numberBN = numberBN.toFixed();
            pricesBN.push(numberBN);
        }

        console.log("Send setPrice TX with: \n");
        console.log(assets);
        console.log(pricesBN);

        let calldata = await this.cfdPriceOracle.methods.setPrice(assets, pricesBN).encodeABI();
        let receipt = await this.sendTx(calldata, cfdPriceOracleADDRESS);

        console.log("TX_HASH: \n");
        console.log(receipt);

        return receipt;
    }

    async checkClose(): Promise<any> {
        let cfds = await this.cfdFactory.methods.getCFDs().call();
        let n_cfds = cfds.length;
        let initial = 0;
        let last = MAX_CHECKS_PER_TX;
        let receipt: any;

        do {
            let calldata = await this.cfdFactory.methods.checkClose(String(initial), String(last)).encodeABI();
            receipt = await this.sendTx(calldata, cfdFactoryADDRESS);

            n_cfds -= MAX_CHECKS_PER_TX;
            initial += MAX_CHECKS_PER_TX;
            last += MAX_CHECKS_PER_TX;

        } while (n_cfds > 0);

        return receipt;
    }

    async killCFD(): Promise<any> {
        let closedCFDs: string[] = [];
        let cfds = await this.cfdFactory.methods.cdfs().call();
        cfds.forEach( async function (address: string) {
            this.cfdContract = new this.web3.eth.Contract(cfdABI, address);
            this.cfdContract.getPastEvents('Close', {
                filter: {},
                fromBlock: 0,
                toBlock: 'latest'
            }, function (error: any, event: any) {
                if (error) {
                    console.log(error);
                } else {
                    if (event.length > 0) {
                        //save info in DB, the contract is going to dead...
                        closedCFDs.push(address);
                    }
                }
            });
        });

        let calldata = await this.cfdFactory.methods.killCFD(closedCFDs).encodeABI();
        let receipt = await this.sendTx(calldata, cfdFactoryADDRESS);

        return receipt;
    }

    async sendTx(calldata: string, contractAddress: string): Promise<any> {
        let privateKey = Buffer.from('9F47B7FDC796C13251731827CC59E0083E5BEC070E7F53EADD54D1DD2BBDBF17', 'hex');
        let signer = "0xff22373340A43CB4b4Ef9697a65f6e19c1F7c892";

        let nonce = await this.web3.eth.getTransactionCount(signer);
        let nonceHex = this.web3.utils.toHex(nonce)
        const gasPrice = 0;
        const gasPriceHex = this.web3.utils.toHex(gasPrice);
        const block = await this.web3.eth.getBlock("latest");
        const gasLimit = 6000000;
        const gasLimitHex = this.web3.utils.toHex(gasLimit);

        let tx = new Tx(
            {
            nonce: nonceHex,
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            to: contractAddress,
            value: '0x00',
            data: calldata
            },
            { common: this.customCommon },
        )

        tx.sign(privateKey);
        let serializedTx = tx.serialize();
        let receipt = await this.web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));//.on('receipt', console.log);
        return receipt;
    }
}
