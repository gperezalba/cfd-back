import { Controller, Post, Get, Body } from "@nestjs/common";

import { CfdsService } from "./cfds.service";
import { PuppeteerService } from "./puppeteer/puppeteer.service";

@Controller('cfds')
export class CfdsController {
    constructor(private readonly cfdsService: CfdsService, 
        private readonly puppeteerService: PuppeteerService) {}

    @Get('getPrice')
    async getPrice(@Body('asset') asset: string): Promise<string> {
        return await this.cfdsService.getPrice(asset);
    }

    @Post('setPrice')
    async setPrice(): Promise<any> {
        let assetsArray: string [] = [];
        let pricesArray: string [] = [];
        let price: string;
        let receipt: any;
        let status: boolean = false;

        while (!status) {
            assetsArray = await this.cfdsService.getAssets();

            for (let i: number = 0; i < assetsArray.length; i++) {
                price = await this.puppeteerService.scrapperPrice(assetsArray[i]);
                pricesArray.push(price);
            }

            receipt = await this.cfdsService.setPrice(assetsArray, pricesArray);
            status = receipt.status;
        }

        return receipt;
    }

    @Post('checkClose')
    async checkClose(): Promise<any> {
        let receipt: any;
        let status: boolean = false;

        while (!status) {
            receipt = await this.cfdsService.checkClose();
            status = receipt.status;
        }

        return receipt;
    }

    @Post('killCFD')
    async killCFD(): Promise<any> {
        let receipt: any;
        let status: boolean = false;

        while (!status) {
            receipt = await this.cfdsService.killCFD();
            status = receipt.status;
        }

        return receipt;
    }
}
