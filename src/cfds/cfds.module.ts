import { Module } from "@nestjs/common";
import { CfdsController } from "./cfds.controller";
import { CfdsService } from "./cfds.service";
import { PuppeteerService } from './puppeteer/puppeteer.service';

@Module({
    controllers: [CfdsController],
    providers: [CfdsService, PuppeteerService],
})
export class CfdsModule {}