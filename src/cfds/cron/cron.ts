const cron = require('node-cron');
const axios = require('axios');

cron.schedule("42 * * * *", async function () {
    console.log("Going to set price... \n");
    await setPrice();
    console.log("Going to check close... \n");
    //await checkClose();
    console.log("Going to kill CFDs... \n");
    //await killCFD();
});

async function setPrice() {
    try {
        const response = await axios.post('http://localhost:8000/cfds/setPrice');
        console.log(response.data);
    } catch (error) {
        console.error(error);
    }
}

async function checkClose() {
    try {
        const response = await axios.post('http://localhost:8000/cfds/checkClose');
        console.log(response.data);
    } catch (error) {
        console.error(error);
    }
}

async function killCFD() {
    try {
        const response = await axios.post('http://localhost:8000/cfds/killCFD');
        console.log(response.data);
    } catch (error) {
        console.error(error);
    }
}