export class Cfd {
    constructor(public seller: string, 
        public asset: string,
        public price: string,
        public investment: string,
        public leverage: string,
        public expiration: string,
        public position: boolean, 
        public address: string) {};
}