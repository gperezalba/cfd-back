import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CfdsModule } from './cfds/cfds.module';

@Module({
  imports: [CfdsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
